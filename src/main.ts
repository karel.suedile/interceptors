import App from './App.svelte';
import 'core-js/stable';
import 'regenerator-runtime/runtime';

const div: HTMLDivElement = document.createElement('div');
document.body.appendChild(div);
const app: App = new App({
    target: div
});

export default app;

import type { ICall } from './types/call';
import type { IMock } from './types/call';

const MOCK_KEY: string = 'ks-mocks';

export function handleConsole(consoleCallbacks: {[index: string]: (...args) => void}) {
    const methods: string[] = ['log', 'warn', 'debug', 'error'];
    for (let i=0; i<methods.length; i++) {
        const method: string = methods[i];

        try {
            const old: Function = console[method];
            console['native-'+method] = old;
            console[method] = function () {
                try {
                    consoleCallbacks[method] && consoleCallbacks[method].apply(this, arguments);
                    return old.apply(this, arguments);
                } catch (e) {
                    old.call(this, ['unprocessable log message']);
                    return undefined;
                }
            }
        } catch (e) {
            // console.log('error on method ' + method);
        }
    }
}

export function handleGlobalErrors(callback: (...args) => void) {
    window.onerror = function (message: Event | string, source: string, lineno: number, colno: number, error: Error) {
        callback.apply(this, arguments);
    };
}

export function handleXhr(xhrCallbacks: {[index: string]: (...args) => void}) {
    const methods: string[] = Object.keys(XMLHttpRequest.prototype).filter(prop => {
        const tmp = new XMLHttpRequest();
        return typeof tmp[prop] === 'function';
    });

    for (let i=0; i<methods.length; i++) {
        const method: string = methods[i];

        try {
            const old: Function = XMLHttpRequest.prototype[method];
            XMLHttpRequest.prototype[method] = function () {
                let result;
                if (!xhrCallbacks['before-' + method] || xhrCallbacks['before-' + method].apply(this, arguments)) {
                    result = old.apply(this, arguments);
                }
                return xhrCallbacks['after-' + method] && xhrCallbacks['after-' + method].call(this, result);
            }
        } catch (e) {
            // console.log('error on method ' + method);
        }
    }
}

export function getMock(call: ICall): IMock {
    const defaultMock: IMock = {
        id: new Date().getTime() + '_' + Math.random(),
        method: call.method,
        url: call.url,
        isRegexp: false,
        regexp: '',
        active: false,
        status: 200,
        headers: 'Content-Type: application/json',
        body: ''
    }
    const item = localStorage.getItem(MOCK_KEY);
    if (!item) {
        return defaultMock;
    }
    const mocks: IMock[] = JSON.parse(item);
    const mock = mocks.find((m: IMock) => {
        if (m.isRegexp) {
            return call.url.match(new RegExp(m.regexp)) && m.method === call.method;
        } else {
            return m.url === call.url && m.method === call.method;
        }
    });
    return mock || defaultMock;
}

export function saveMock(mock: IMock): void {
    const item = localStorage.getItem(MOCK_KEY);
    if (!item && mock.active === false && mock.body === '') {
        return;
    }

    let mocks: IMock[] = item ? JSON.parse(item) : [];
    const savedMock = mocks.find((m: IMock) => m.id === mock.id);
    if (savedMock) {
        savedMock.active = mock.active;
        savedMock.status = mock.status;
        savedMock.headers = mock.headers;
        savedMock.body = mock.body;
        savedMock.regexp = mock.regexp;
        savedMock.isRegexp = mock.isRegexp;
    } else {
        mocks = mocks.concat([mock]);
    }

    localStorage.setItem(MOCK_KEY, JSON.stringify(mocks));
}

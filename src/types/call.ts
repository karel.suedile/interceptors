class IHeader {
    name: string;
    value: string;
}

export class IMock {
    id: string;
    method: string;
    url: string;
    isRegexp: boolean;
    regexp: string;
    active: boolean;
    status: number;
    headers: string;
    body: string;
}

export class ICall {
    method: string;
    url: string;
    endUrl: string;
    request: {
        headers: IHeader[];
        payload: string;
    };
    response: {
        headers: IHeader[];
        body: string;
        status: number;
        statusText: string;
    };
    time: number;
    error: boolean;
    mock: IMock;
    isMocked: boolean;
}


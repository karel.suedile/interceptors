# Interceptors

A devtools project with console and network for monitoring XmlHttpRequests and console messages, juste like chrome devtools.

It is a small firebug-lite alternative written with svelte.

To get it work, add the following lines to your html head section :

```bash
<link rel='stylesheet' href="https://storage.googleapis.com/appfees/interceptors/bundle.css?timestamp=TIMESTAMP">
<script defer src="https://storage.googleapis.com/appfees/interceptors/bundle.js?timestamp=TIMESTAMP"></script>
```

Then, in these two lines, replace TIMESTAMP by some value, e.g.: 2021-03-21_03:38:00

You should see a * in the bottom left edge of your web page.

Click on it to toggle the devtools panel.
